package ru.sokolov;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите имя:");
        String name = scanner.nextLine();

        System.out.println("Введите возраст:");
        int age = scanner.nextInt();

        System.out.println("Введите сумму кредита:");
        double creditSum = scanner.nextDouble();

        if(name.equals("Bob")){
            System.out.println("We do not give credit to Bob!");
        } else if(age < 18){
            System.out.println("You are not the right age!");
        } else if(creditSum > age * 100){
            System.out.println("You have been approved for a loan - " + age * 100);
        } else {
            System.out.println("You have been approved for a loan - " + creditSum);
        }

    }
}
